#!/bin/bash

DIR="$(cd "$(dirname "$0")" && pwd)"
echo $DIR

ln -sf "$DIR/.bash_profile" ~/.bash_profile
ln -sf "$DIR/.bash_prompt" ~/.bash_prompt
ln -sf "$DIR/.vimrc" ~/.vimrc


