echo "Running .bash_profile"

# IMPORTS
if [ -f $HOME/.bash_functions ]; then
  . $HOME/.bash_functions
fi

source  $HOME/.bash_prompt

# ALIAS
alias ll='ls -lah --color=auto'
alias grep='grep --color'
alias dc='docker-compose'
alias gg='git grep'

vimgitgrep(){ vim $(git grep -l $1); }
alias vg='vimgitgrep'

# ENV VARS
export EDITOR="vim"


# OTHER
# install vim plugins
if [ ! -d "$HOME/.vim/plugin" ]; then
  echo "Installing Vim Grep"
  mkdir -p $HOME/.vim/plugin

  #Vim Grep
  curl -Sso $HOME/.vim/plugin/grep.vim https://raw.githubusercontent.com/vim-scripts/grep.vim/master/plugin/grep.vim
fi

# run user .profile file on mac
[[ $OSTYPE == darwin* ]] &&  [[ -e $HOME/.profile ]] &&  source $HOME/.profile
