" Show Line Numbers
set number

" Enable Syntax Highlighting
syntax on

" Auto-Indenting based on file type
filetype indent plugin on

" Use case insensitive search, except when using capital letters
set ignorecase
set smartcase

" search as characters are entered
set incsearch

" highlight matches 
set hlsearch

" Enable Mouse in all modes
set mouse=a

" Indentation spaces
set shiftwidth=2
set softtabstop=2
set expandtab

" Show file stats at bottom
set ruler

" Highlight current line
set cursorline

" Show autocomplete menu visually e.g. :e ~/.vim<TAB>
set wildmenu

" Set to auto read when a file is changed from the outside
set autoread

" :W sudo saves the file
" (useful for handling the permission-denied error)
command W w !sudo tee % > /dev/null

" Turn backup off, since most stuff is in SVN, git et.c anyway...
set nobackup
set nowb
set noswapfile

" Highlight Whitespace at End of Line
match Error /\s\+$/


" Source .vimrc file in present working directory (proj specific config)
set exrc
" restrict usage of commands from local .vimrc file
set secure

