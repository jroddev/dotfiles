**DOT FILES**
 A repo for storing dotfiles for quick commandline configuration  
  
**install.sh**  
- Run this to install dotfiles  
- **WARNING** this will just overwrite existing files in HOME directory  
  
**.bash_profile**  
- Main File. Loaded when a new bash session is started  
- Add custom setup to .bashrc file. It will be called at the end of this file  
  
**.bash_prompt**  
- Custom prompt configuration  
- git branch support  
- ssh username support  
  
**.vimrc**  
- Custom vim configuration  

